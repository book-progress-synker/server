CREATE TABLE IF NOT EXISTS books(
filename text not null primary key UNIQUE,
progress real not null,
last_opened text not null
);
