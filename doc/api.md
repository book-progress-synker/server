# Api documentation

## Datatypes
### rust
#### Book
Most of time will be sended this structure as response.
``` rust
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Book {
    id: String,
    filename: String,
    progress: Progress,
    last_opened: DateTime<Local>,
}
```

#### BookBuilder 
This structure what will be sended by you to server. \
Date should be in "%Y-%m-%d %H:%M:%S.3f %z" format, exactly that will be sended
from server in case of wrong format.
``` rust
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct BookBuilder {
    id: Option<String>,
    filename: String,
    progress: f64,
    last_opened: String,
}
```
### json
Data sended in json via http protocol.

#### Json response examples:

##### Book

``` json
{
  "id": "8a5909ca-aa14-7dba-30ce-a5c8eec2f061",
  "filename": "test.pdf",
  "progress": {
    "val": 0.0
  },
  "last_opened": "2024-02-28T15:41:06+03:00"
}
```
API respond in already builded book. \

##### BookBuilder
BookBuilder has possibility to generate id, in case if not necessary to add with
certain id, id can be dropped (this possible with help of Option<String> type and serde)
``` json
{
  "filename":"test.pdf",
  "progress":0.0,
  "last_opened":"2024-02-28 15:41:06 +0300"
}
```

## Requests
### fetch all books
GET: /api/v1/books
`curl 127.0.0.1:8080/api/v1/books -s | jq`

### fetch book by id
GET: /api/v1/books/{id} \
`curl 127.0.0.1:8080/api/v1/books/8a5909ca-aa14-7dba-30ce-a5c8eec2f061 -s | jq` \
where `8a5909ca-aa14-7dba-30ce-a5c8eec2f061` is id

### add book
POST: /api/v1/books \
data: BookBuilder in json format \
``` shell
curl --request POST --data \
  '{
    "filename":"test.pdf",
    "progress":0.0,
    "last_opened":"2024-02-28 15:41:06 +0300"
  }' \
  http://localhost:8080/api/v1/books
```

### update book
PUT: /api/v1/books/ \
data: String
data: BookBuilder in json format \
``` shell
curl --request PUT --data \
  '{
    "id":"8a5909ca-aa14-7dba-30ce-a5c8eec2f061"
    "filename":"test.pdf",
    "progress":0.0,
    "last_opened":"2024-02-28 15:41:06 +0300"
  }' \
  http://localhost:8080/api/v1/books
```

### delete book
DELETE: /api/v1/books/{id}
`curl --request DELETE 127.0.0.1:8080/api/v1/books/8a5909ca-aa14-7dba-30ce-a5c8eec2f061`

### print config yaml
will gets you refference config in yaml \
GET: /api/v1/config/default/yaml
`curl 127.0.0.1:8080//api/v1/config/default/yaml`

### print config json
will gets you refference config in json \
GET: /api/v1/config/default
`curl 127.0.0.1:8080/api/v1/config/default -s | jq`
