# Server config

Here is default config should be placed in file with name config.yaml for yaml or config.json for json

``` yaml
port: 8080
data_dir: /home/user/.local/share/bprogsynksrv/
db_type: !Sqlite
  path: /home/user/.local/share/bprogsynksrv/db.sqlite
tls_enabled: true
```

``` json
{
  "port": 8080,
  "data_dir": "/home/user/.local/share/bprogsynksrv/",
  "db_type": {
    "Sqlite": {
      "path": "/home/user/.local/share/bprogsynksrv/db.sqlite"
    }
  },
  "tls_enabled": true
}
```

