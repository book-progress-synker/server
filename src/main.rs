use actix_web::{delete, error, get, post, put, web, App, HttpResponse, HttpServer, Responder};
use lazy_static::lazy_static;
use serde_json;
use serde_yaml;

mod db;
mod settings;
mod state;

use datatypes::BookBuilder;
use db::DataBase;
use settings::Settings;
// use state::AppState;

lazy_static! {
    static ref SETTINGS: Settings = Settings::init();
}

fn error_message(error_message: String) -> String {
    format!("{{\"err\": \"{}\"}}", error_message)
}

/// in theory you will can try to modify this by put requests but in fact
/// it will used to create yaml template but later... probably...
#[get("/api/v1/config/default")]
async fn print_config_json() -> impl Responder {
    let response: String = match serde_json::to_string(&Settings::default()) {
        Ok(val) => val,
        Err(e) => error_message(e.to_string()),
    };
    HttpResponse::Ok().body(response)
}

#[get("/api/v1/config/default/yaml")]
async fn print_config_yaml() -> impl Responder {
    // let response = "test";
    let response: String = match serde_yaml::to_string(&Settings::default()) {
        Ok(val) => val,
        Err(e) => error_message(e.to_string()),
    };
    HttpResponse::Ok().body(response)
}

#[get("/api/v1/books")]
async fn fetch_all_books(state: web::Data<Box<dyn DataBase>>) -> impl Responder {
    match &state.get_all_books().await.unwrap() {
        Ok(books) => HttpResponse::Ok().body(serde_json::to_string(books).expect("json books")),
        Err(e) => HttpResponse::BadRequest().body(error_message(e.to_string())),
    }
}

#[get("/api/v1/books/{filename}")]
async fn fetch_book(
    state: web::Data<Box<dyn DataBase>>,
    filename: web::Path<String>,
) -> impl Responder {
    match &state.get_book_by_filename(&filename).await.unwrap() {
        Ok(possible_book) => match possible_book {
            Some(book) => HttpResponse::Ok().body(serde_json::to_string(book).unwrap()),
            None => {
                HttpResponse::BadRequest().body(error_message(String::from("there no such book")))
            }
        },
        Err(e) => HttpResponse::BadRequest().body(error_message(e.to_string())),
    }
}

#[post("/api/v1/books")]
async fn add_book(state: web::Data<Box<dyn DataBase>>, data: String) -> impl Responder {
    // done this way because serde parses this data without errors,
    // probably i don't understand how actix parse data
    match serde_json::from_str(&data) as Result<BookBuilder, serde_json::Error> {
        Ok(book) => match book.build() {
            Ok(book) => {
                let db = state;
                match &db.add_book(book).await.unwrap() {
                    Ok(()) => return HttpResponse::Ok().body(""),
                    Err(e) => return HttpResponse::BadRequest().body(error_message(e.to_string())),
                }
            }
            Err(e) => {
                return HttpResponse::BadRequest().body(error_message(e.to_string()));
                // eprint!("{}", E.to_string())
            }
        },
        Err(e) => return HttpResponse::BadRequest().body(error_message(e.to_string())),
    }
}

#[put("/api/v1/books")]
async fn update_book(state: web::Data<Box<dyn DataBase>>, data: String) -> impl Responder {
    match serde_json::from_str(&data) as Result<BookBuilder, serde_json::Error> {
        Ok(book) => match book.build() {
            Ok(book) => {
                let db = state;
                match &db.update_book(&book).await.unwrap() {
                    Ok(()) => return HttpResponse::Ok().body(""),
                    Err(e) => return HttpResponse::BadRequest().body(error_message(e.to_string())),
                }
            }
            Err(e) => {
                return HttpResponse::BadRequest().body(error_message(e.to_string()));
                // eprint!("{}", E.to_string())
            }
        },
        Err(e) => return HttpResponse::BadRequest().body(error_message(e.to_string())),
    }
}

// TODO: try to combine match serde to book cause too much common logic between this functions
#[delete("/api/v1/books/{filename}")]
async fn delete_book(
    state: web::Data<Box<dyn DataBase>>,
    filename: web::Path<String>,
) -> impl Responder {
    let db = state;
    match &db.delete_book(&filename).await.unwrap() {
        Ok(()) => return HttpResponse::Ok().body(""),
        Err(e) => return HttpResponse::BadRequest().body(error_message(e.to_string())),
    }
}

#[tokio::main]
async fn main() -> std::io::Result<()> {
    // std::env::set_var("RUST_LOG", "debug");
    // env_logger::init();

    HttpServer::new(move || {
        let json_config = web::JsonConfig::default()
            .limit(4096)
            .error_handler(|err, _req| {
                // create custom error response
                error::InternalError::from_response(err, HttpResponse::Conflict().finish()).into()
            });
        App::new()
            .app_data(json_config)
            .data_factory(|| async { Ok::<_, ()>(SETTINGS.create_db().await) })
            .service(print_config_json)
            .service(print_config_yaml)
            .service(fetch_all_books)
            .service(fetch_book)
            .service(add_book)
            .service(update_book)
            .service(delete_book)
    })
    .bind(("127.0.0.1", SETTINGS.get_port()))?
    .run()
    .await
}
