use crate::db::sqlitebuilder::SqliteBuilder;
use crate::db::DataBase;
use crate::db::DbType;
use dirs;
use serde::{Deserialize, Serialize};
use serde_yaml;
use std::env;
use std::path::PathBuf;
use tokio::task;
use tokio::task::JoinHandle;

#[derive(Deserialize, Serialize)]
pub struct SettingsBuilder {
    port: Option<u16>,
    /// by default dirs::data_local_dir()/bprogsynksrv (~/.local/share/brogsynksrv)
    data_dir: Option<PathBuf>,
    db_type: Option<DbType>,
    tls_enabled: Option<bool>,
}

impl SettingsBuilder {
    pub fn parse_settings_from_file() -> Settings {
        let mut config_dir = dirs::config_dir().unwrap();
        config_dir.push("bprogsynksrv/");
        let mut config_dir2 = dirs::home_dir().unwrap();
        config_dir2.push(".bprogsynksrv/");
        let mut config_dir3 = dirs::data_local_dir().unwrap();
        config_dir3.push("bprogsynksrv/");
        // HACK: will work properly everywhere except... one certain os which uses \ for path
        let mut project_dir = env::current_exe().unwrap().to_path_buf();
        project_dir.set_file_name("");
        project_dir = project_dir
            .to_str()
            .unwrap()
            .trim_end_matches("target/debug/")
            .into();
        let mut config_dirs = vec![project_dir, config_dir, config_dir2, config_dir3];
        for path in &mut config_dirs {
            if path.exists() {
                path.push("config.yaml");
                // TODO: del prints someday
                if path.exists() {
                    println!("{}", path.clone().to_str().unwrap());
                    return Self::read_yaml_settings(path);
                }
                path.set_file_name("config.yml");
                if path.exists() {
                    println!("{}", path.clone().to_str().unwrap());
                    return Self::read_yaml_settings(path);
                }
                // for pervs who like use json for configs
                path.set_file_name("config.json");
                if path.exists() {
                    println!("{}", path.clone().to_str().unwrap());
                    return Self::read_json_settings(path);
                }
            }
        }
        println!("app started with default settings");
        Settings::default()
    }

    fn read_json_settings(path: &PathBuf) -> Settings {
        let f = std::fs::File::open(path).expect("Could not open file.");
        let settings: SettingsBuilder = serde_json::from_reader(f).expect("Could not read values.");
        return settings.build();
    }

    fn read_yaml_settings(path: &PathBuf) -> Settings {
        let f = std::fs::File::open(path).expect("Could not open file.");
        let settings: SettingsBuilder = serde_yaml::from_reader(f).expect("Could not read values.");
        return settings.build();
    }

    fn build(self) -> Settings {
        let default = Settings::default();
        Settings {
            port: match self.port {
                Some(port) => port,
                None => default.port,
            },
            data_dir: match self.data_dir {
                Some(data_dir) => normalize_path(data_dir),
                None => default.data_dir,
            },
            db_type: match self.db_type {
                Some(db_type) => db_type,
                None => default.db_type,
            },
            tls_enabled: match self.tls_enabled {
                Some(tls_enabled) => tls_enabled,
                None => default.tls_enabled,
            },
        }
    }
}

#[derive(Deserialize, Serialize)]
pub struct Settings {
    pub port: u16,
    pub data_dir: PathBuf,
    pub db_type: DbType,
    pub tls_enabled: bool,
}

impl Settings {
    pub fn init() -> Settings {
        SettingsBuilder::parse_settings_from_file()
    }

    pub fn default() -> Settings {
        let mut data_dir = dirs::data_local_dir().expect("data dir");
        data_dir.push("bprogsynksrv/");
        if !data_dir.exists() {
            std::fs::create_dir(data_dir.clone()).expect("created data dir");
        }
        // let mut db_path = data_dir.clone();
        // db_path.push("db.sqlite");
        Settings {
            port: 8080,
            data_dir,
            db_type: DbType::Sqlite(SqliteBuilder::new("db.sqlite".to_string())),
            tls_enabled: true,
        }
    }

    pub fn get_datadir(&self) -> PathBuf {
        self.data_dir.clone()
    }

    pub fn get_port(&self) -> u16 {
        self.port
    }

    // pub async fn create_db(&self) -> JoinHandle<Box<dyn DataBase>> {
    pub async fn create_db(&self) -> Box<dyn DataBase> {
        match &self.db_type {
            DbType::Sqlite(builder) => {
                // let ret: JoinHandle<Box<dyn DataBase>> =
                //     task::spawn(async {
                Box::new(builder.connect(&self).await.unwrap())
                //     });
                // ret
            }
            DbType::MongoDB(builder) => {
                // task::spawn(async {
                Box::new(builder.connect().await.unwrap())
                // })
            }
            DbType::Json(builder) => {
                // tokio::spawn(async {
                eprintln!("Json was build for test avoid of using it");
                Box::new(builder.read(&self))
                // })
            }
            _ => unimplemented!("create db not implemented for this type"),
        }
    }

    // pub async fn create_db(&self) -> JoinHandle<impl DataBase> {
    //     match &self.db_type {
    //         DbType::Sqlite(builder) => {
    //             let ret: JoinHandle<Box<dyn DataBase>>= task::spawn(async { builder.connect(&self).await.unwrap() });
    //             ret
    //         }
    //         DbType::MongoDB(builder) => Box::new(builder.connect().await),
    //         DbType::Json(builder) => Box::new(builder.read()),
    //     }
    // }
}

fn normalize_path(path: PathBuf) -> PathBuf {
    let p = path.to_str().unwrap();
    if p.contains('~') {
        let new_path = PathBuf::from(p.replace("~", dirs::home_dir().expect("valid path").to_str().unwrap()));
        return new_path;
    }
    return path;
}
