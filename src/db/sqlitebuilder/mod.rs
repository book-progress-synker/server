use crate::db::DataBase;
use crate::Settings;
use datatypes::Book;
use datatypes::BookBuilder;
// use rusqlite::Connection;
use serde::{Deserialize, Serialize};
// use sqlx::Connection;
use sqlx::sqlite::SqlitePool;
use std::path::PathBuf;
use tokio;
use tokio::task::JoinHandle;
// use tokio_rusqlite::Connection;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct SqliteBuilder {
    filename: Option<String>,
}

impl SqliteBuilder {
    /// in fact build function
    pub async fn connect(&self, settings: &Settings) -> Result<SqliteDb, super::DbErr> {
        let mut path = settings.data_dir.clone();
        if self.filename.is_some() {
            path.push(self.filename.clone().unwrap());
        } else {
            path.push("db.sqlite");
        }

        // TODO: del print when ready or make it warn(env loging) or smth like that
        println!("{}", path.clone().to_str().unwrap());
        let pool = SqlitePool::connect(path.to_str().expect("valid path"))
            // open(path.to_str().expect("valid path"))
            .await?;

        let mut conn = pool.acquire().await?;

        sqlx::query(
            r#"
CREATE TABLE IF NOT EXISTS books(
filename text not null primary key UNIQUE,
progress real not null,
last_opened text not null
);
            "#,
        )
        .execute(&mut *conn)
        .await?;
        Ok(SqliteDb { pool })
    }

    /// used only for default config (case if no config at all)
    pub fn new(filename: String) -> SqliteBuilder {
        SqliteBuilder { filename: Some(filename) }
    }
}

#[derive(Debug)]
pub struct SqliteDb {
    pool: SqlitePool,
}

impl SqliteDb {
    // fn get_books(poll: Poll) -> JoinHandle<Result<Vec<Book>, super::DbErr>> {
    //     tokio::spawn(async move {
    //         let conn = poll.aqu
    //         Ok(conn
    //             .call(move |conn| {
    //                 // let mut books_table =
    //                 //     conn.execute("SELECT id, filename, progress, last_opened FROM books;", [])?;
    //                 let mut books_table =
    //                     conn.prepare("SELECT id, filename, progress, last_opened FROM books;")?;
    //                 // let books = books_table
    //                 let books =
    //                     books_table
    //                         .query_map([], |row| {
    //                             Ok(BookBuilder::new(
    //                                 row.get(0)?,
    //                                 row.get(1)?,
    //                                 row.get(2)?,
    //                                 row.get(3)?,
    //                             )
    //                             .build()
    //                             .unwrap())
    //                         })
    //                         .unwrap();
    //                 Ok(books)
    //             })
    //             .await
    //             .unwrap()
    //             .into_iter()
    //             .map(|book| book.unwrap())
    //             .collect())
    //     })
    // }
}

impl DataBase for SqliteDb {
    fn get_all_books(&self) -> JoinHandle<Result<Vec<Book>, super::DbErr>> {
        let conn_future = self.pool.acquire();
        // TODO: separate this to independent SQL module and pass only futures with pools
        //       to support all sqlx databases (Postgress, mysql, etc.)
        //       example annotation for this function:
        //       fn get_all_books(conn_future: impl Future<Output = Result<PoolConnection<DB>, Error>> + 'static)
        //           -> JoinHandle<Result<Vec<Book>, super::DbErr>>
        //       or something like that.
        tokio::spawn(async move {
            let mut conn = conn_future.await?;
            let books_table =
                sqlx::query!("SELECT filename, progress, last_opened FROM books;")
                    .fetch_all(&mut *conn)
                    .await?;
            let books = books_table
                .into_iter()
                .map(|rec| {
                    BookBuilder::new(
                        rec.filename,
                        rec.progress,
                        rec.last_opened,
                    )
                    .build()
                    .unwrap()
                })
                .collect();
            Ok(books)
        })
    }

    fn get_book_by_filename(&self, filename: &String) -> JoinHandle<Result<Option<Book>, super::DbErr>> {
        let filename = filename.clone();
        let conn_future = self.pool.acquire();
        tokio::spawn(async move {
            let mut conn = conn_future.await?;
            let book_rec = sqlx::query!(
                "SELECT filename, progress, last_opened FROM books WHERE filename = ?1",
                filename
            )
            .fetch_one(&mut *conn)
            .await?;
            let book = BookBuilder::new(
                book_rec.filename,
                book_rec.progress,
                book_rec.last_opened,
            )
            .build()
            .unwrap();
            Ok(Some(book))
        })
    }

    fn add_book(&self, book: Book) -> JoinHandle<Result<(), super::DbErr>> {
        let conn_future = self.pool.acquire();
        let filename = book.get_filename();
        let progress = book.get_progress().get_value();
        let last_opened = book.get_last_opened().to_string();
        tokio::spawn(async move {
            let mut conn = conn_future.await?;
            sqlx::query!(
                "INSERT INTO books(filename, progress, last_opened) VALUES (?1, ?2, ?3)",
                filename,
                progress,
                last_opened
            )
            .execute(&mut *conn)
            .await?;
            Ok(())
        })
    }

    fn update_book(&self, updated_book: &Book) -> JoinHandle<Result<(), super::DbErr>> {
        // let book = updated_book.clone();
        let filename = updated_book.get_filename();
        let progress = updated_book.get_progress().get_value();
        let last_opened = updated_book.get_last_opened().to_string();
        let conn_future = self.pool.acquire();
        tokio::spawn(async move {
            let mut conn = conn_future.await?;
            sqlx::query!(
                r#"
UPDATE books
  SET
    progress = ?2,
    last_opened = ?3
  WHERE filename = ?1;
"#,
                filename,
                progress,
                last_opened
            )
            .execute(&mut *conn)
            .await?;
            Ok(())
        })
    }

    fn update_book_progress(
        &self,
        filename: String,
        book_progress: datatypes::Progress,
    ) -> JoinHandle<Result<(), super::DbErr>> {
        let filename = filename.clone();
        let progress = book_progress.get_value();
        let conn_future = self.pool.acquire();
        tokio::spawn(async move {
            let mut conn = conn_future.await?;
            sqlx::query!(
                r#"
UPDATE books
  SET
    progress = ?2
  WHERE filename = ?1;
"#,
                filename,
                progress,
            )
            .execute(&mut *conn)
            .await?;
            Ok(())
        })
    }

    fn delete_book(&self, filename: &String) -> JoinHandle<Result<(), super::DbErr>> {
        let filename = filename.clone();
        let conn_future = self.pool.acquire();
        tokio::spawn(async move {
            let mut conn = conn_future.await?;
            sqlx::query!("DELETE FROM books WHERE filename = ?1;", filename,)
                .execute(&mut *conn)
                .await?;
            Ok(())
        })
    }
}
