use datatypes::Book;
use datatypes::Progress;
use serde::{Deserialize, Serialize};
use sqlx;
use thiserror::Error;
// use std::path::PathBuf;
use tokio::task::JoinHandle;

pub mod jsonbuilder;
pub mod mongobuilder;
pub mod sqlitebuilder;

use jsonbuilder::JsonBuilder;
use mongobuilder::MongoDBBuilder;
use sqlitebuilder::SqliteBuilder;

/// Builders should be defined in settings
#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum DbType {
    Sqlite(SqliteBuilder),
    MongoDB(MongoDBBuilder),
    Json(JsonBuilder),
}

// pub enum DbConnection {
//     Sqlite(Connection),
//     MongoDB(Client),
//     Json(JsonBooks),
// }

// pub struct DB {
//     pub connection: DbConnection,
// }

// impl DB {
//     pub fn new(db_type: DbType) -> DB {
//         let connection: DbConnection;
//         match db_type {
//             DbType::Sqlite(builder) => connection = DbConnection::Sqlite(builder.connect()),
//             DbType::MongoDB(builder) => connection = DbConnection::MongoDB(builder.connect()),
//             DbType::Json(builder) => connection = DbConnection::Json(builder.read()),
//         }
//         DB { connection }
//     }
// }

#[derive(Error, Debug)]
pub enum DbErr {
    // #[error("failed convert to progress structure")]
    // Progress(#[from] ProgressErr),
    #[error("failed to create sqlite connection")]
    SqliteConnection(#[from] sqlx::Error),
    #[error("failed to build book")]
    BookBuild(#[from] datatypes::BookErr),
    #[error("failed to write json {0}")]
    JsonWrite(String),
    // #[error("unknown data store error")]
    // Unknown,
}

/// named this way because of mongodb Database... probably should be renamed
pub trait DataBase {
    fn get_all_books(&self) -> JoinHandle<Result<Vec<Book>, DbErr>> {
        todo!("implement get all books")
    }

    fn get_book_by_filename(&self, filename: &String) -> JoinHandle<Result<Option<Book>, DbErr>> {
        todo!("implement get books")
    }

    fn add_book(&self, book: Book) -> JoinHandle<Result<(), DbErr>> {
        todo!("add book")
    }

    fn update_book(&self, updated_book: &Book) -> JoinHandle<Result<(), DbErr>> {
        todo!("edit book")
    }

    fn update_book_progress(
        &self,
        filename: String,
        book_progress: Progress,
    ) -> JoinHandle<Result<(), DbErr>> {
        todo!("edit book progress")
    }

    fn delete_book(&self, filename: &String) -> JoinHandle<Result<(), DbErr>> {
        todo!("delete book")
    }
}
