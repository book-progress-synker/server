// use mongodb::synk::Client;
use crate::DataBase;
use mongodb::Client;
use serde::{Deserialize, Serialize};
use tokio::task::JoinHandle;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct MongoDBBuilder {
    uri: String,
    collection: String,
}

impl MongoDBBuilder {
    pub fn connect(&self) -> JoinHandle<MongoDb> {
        let uri = self.uri.clone();
        tokio::spawn(async {
            let client = Client::with_uri_str(uri).await.unwrap();
            MongoDb { client }
        })
    }
}

#[derive(Debug, Clone)]
pub struct MongoDb {
    client: Client,
}

impl DataBase for MongoDb {
    // fn get_all_books(&self) -> Vec<Book> {
    //     unimplemented!()
    // }
}
