use super::DbErr;
use crate::db::DataBase;
use crate::settings::Settings;
use datatypes::Book;
use serde::{Deserialize, Serialize};
use serde_json;
use std::fs;
use std::path::PathBuf;
use std::sync::Mutex;
use tokio::task::JoinHandle;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct JsonBuilder {
    filename: Option<String>,
}

/// dumb implementation to avoid threadsafety issues
impl JsonBuilder {
    pub fn read(&self, settings: &Settings) -> JsonBooks {
        // eprintln!("ITS READS JSON <AMOUNT OF CORES> TIMES");
        let mut path = settings.get_datadir();

        match &self.filename {
            Some(filename) => path.set_file_name(filename),
            None => path.set_file_name("data.json"),
        }

        if !path.exists() {
            match fs::write(path.clone(), "[]") {
                Ok(_) => {}
                Err(e) => {
                    eprintln!(
                        "failed to write to {} for {e} reason",
                        path.to_str().expect("failed convert pathbuf to string")
                    )
                }
            }
        }
        let json = fs::read_to_string(path.clone()).expect("valid json data");
        let books: Vec<Book> = serde_json::from_str(&json).expect("books vector");
        JsonBooks {
            // books,
            books: Mutex::new(books),
            path: path.clone(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct JsonBooks {
    books: Mutex<Vec<Book>>,
    path: PathBuf,
}

impl JsonBooks {
    pub fn save(&self) -> JoinHandle<Result<(), DbErr>> {
        let get_books_future = self.get_all_books();
        let path = self.path.clone();
        tokio::spawn(async move {
            // let books = self.get_all_books().await.unwrap();
            // let to_save =
            //     serde_json::to_string(&books.unwrap()).unwrap_or(String::from("[]"));
            // println!("{to_save}");
            match fs::write(
                path.clone(),
                // serde_json::to_string_pretty(&books).expect("empty book vec"),
                serde_json::to_string(&get_books_future.await.unwrap().unwrap())
                    .unwrap_or(String::from("[]")), // to_save,
            ) {
                Ok(_) => Ok(()),
                Err(_) => {
                    Err(DbErr::JsonWrite(path.to_str().unwrap().to_string()))
                    // eprintln!(
                    //     "failed to write to {} for {e} reason",
                    //     self.path
                    //         .to_str()
                    //         .expect("failed convert pathbuf to string")
                    // )
                }
            }
        })
    }
}

impl DataBase for JsonBooks {
    fn get_all_books(&self) -> tokio::task::JoinHandle<Result<Vec<Book>, DbErr>> {
        // TODO: seems too not optimezed...
        //but i dont plan use it anyway, maybe later i'll rewrite this part
        let books = self.books.lock().unwrap();
        let books = books.clone();
        tokio::spawn(async { Ok(books) })
    }
    // async fn get_all_books(&self) -> Result<Vec<Book>, super::DbErr> {
    // }

    // async fn add_book(&self, book: Book) -> Result<Book, super::DbErr> {
    //     let mut books = self.books.lock().unwrap();
    //     books.push(book.clone());
    //     drop(books);
    //     self.save();
    //     Ok(book)
    // }
}
